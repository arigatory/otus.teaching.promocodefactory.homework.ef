﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly AppDbContext _appDbContext;

        public EfDbInitializer(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public void InitializeDb()
        {
            //_appDbContext.Database.EnsureDeleted();
            //_appDbContext.Database.EnsureCreated();

            _appDbContext.AddRange(FakeDataFactory.Employees);
            _appDbContext.SaveChanges();

            _appDbContext.AddRange(FakeDataFactory.Preferences);
            _appDbContext.SaveChanges();

            _appDbContext.AddRange(FakeDataFactory.Customers);
            _appDbContext.SaveChanges();
        }
    }
}
