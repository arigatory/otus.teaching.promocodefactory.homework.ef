﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> 
        where T : BaseEntity

    {
        private readonly AppDbContext _appDbContext;

        public EfRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _appDbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] properties)
        {
            var entity = await _appDbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

            if (properties == null)
            {
                return entity;
            }
            var query = _appDbContext.Set<T>() as IQueryable<T>;

            query = properties.Aggregate(query, (current, property) => current.Include(property));
            

            return query.FirstOrDefault(x => x.Id == id);
        }

        public async Task<T> AddAsync(T entity)
        {
            await _appDbContext.Set<T>().AddAsync(entity);
            await _appDbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<bool> UpdateAsync(T entity)
        {
            return await _appDbContext.SaveChangesAsync() > 0;

        }

        public async Task<bool> DeleteAsync(T entity)
        {
            _appDbContext.Set<T>().Remove(entity);
            return await _appDbContext.SaveChangesAsync() > 0;
        }
    }
}
