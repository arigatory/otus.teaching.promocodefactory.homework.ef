﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IMapper _mapper;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly LinkGenerator _linkGenerator;

        public CustomersController(IRepository<Customer> cusotmerRepository, 
            IMapper mapper, 
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository,
            LinkGenerator linkGenerator)
        {
            _customerRepository = cusotmerRepository;
            _mapper = mapper;
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _linkGenerator = linkGenerator;
        }


        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse[]>> Get()
        {
            try
            {
                var customers = await _customerRepository.GetAllAsync();

                return _mapper.Map<CustomerShortResponse[]>(customers);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Проблема с базой данных");
            }
        }
        

        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> Get(Guid id)
        {
            try
            {
                var customer = await _customerRepository.GetByIdAsync(id, c => c.CustomerPreferences, c => c.PromoCodes);

                if (customer == null) return NotFound();

                return _mapper.Map<CustomerResponse>(customer);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Проблема с базой данных");
            }
        }
        

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            try
            {
                var existing = await _customerRepository.GetByIdAsync(request.Id);
                if (existing != null)
                {
                    return BadRequest("Клиент с таким Id уже есть!");
                }

                var location = _linkGenerator.GetPathByAction("Get", 
                    "Customers",
                    new {id = request.Id});
                var customer = _mapper.Map<Customer>(request);

                if (string.IsNullOrWhiteSpace(location))
                {
                    return BadRequest("Нельзя использовать данный id");
                }

                if (await _customerRepository.AddAsync(customer) != null)
                {
                    return Created(location, _mapper.Map<CreateOrEditCustomerRequest>(customer));
                }
                
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Проблема с базой данных");
            }
        }
        
        /// <summary>
        /// Изменить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            try
            {
                var oldCutomer = await _customerRepository.GetByIdAsync(id);
                if (oldCutomer == null) return NotFound($"Не удалось найти клиента с таким id: {id}");

                _mapper.Map(request, oldCutomer);

                await _customerRepository.UpdateAsync(oldCutomer);

                return _mapper.Map<CustomerResponse>(oldCutomer);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Проблема с базой данных");
            }
        }
        
        /// <summary>
        /// Удалить клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            try
            {
                var oldCutomer = await _customerRepository.GetByIdAsync(id);
                if (oldCutomer == null) return NotFound($"Не удалось найти клиента с таким id: {id}");

                if (await _customerRepository.DeleteAsync(oldCutomer))
                {
                    return Ok();
                }
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Проблема с базой данных");
            }
            
            return BadRequest("Не удалось удалить клиента");
        }
    }
}