﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceProfile : Profile
    {
        public PreferenceProfile()
        {
            this.CreateMap<Preference, PreferenceResponse>().ReverseMap();
        }
    }
}
