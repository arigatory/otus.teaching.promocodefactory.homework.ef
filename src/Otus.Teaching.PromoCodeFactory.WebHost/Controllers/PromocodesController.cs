﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IMapper _mapper;

        public PromoCodesController(IRepository<PromoCode> promoCodeRepository,
            IMapper mapper)
        {
            _promoCodeRepository = promoCodeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PromoCodeShortResponse[]>> GetPromocodesAsync()
        {
            try
            {
                var results = await _promoCodeRepository.GetAllAsync();

                return _mapper.Map<PromoCodeShortResponse[]>(results);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Проблема с базой данных");
            }
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: пока не знаю как
            var promoCode = new PromoCode
            {
                Id = new Guid(),
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(7),
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                Preference = new Preference { Name = request.Preference},
                ServiceInfo = request.ServiceInfo
            };
            await _promoCodeRepository.AddAsync(promoCode);
            return Ok();
        }
    }
}