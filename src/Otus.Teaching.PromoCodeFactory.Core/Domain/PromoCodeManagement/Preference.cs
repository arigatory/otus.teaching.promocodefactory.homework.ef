﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public Preference()
        {
            Customers = new HashSet<CustomerPreference>();
        }

        [MaxLength(50)]
        public string Name { get; set; }

        public virtual ICollection<CustomerPreference> Customers { get; set; }

    }
}