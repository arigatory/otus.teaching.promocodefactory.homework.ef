﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public Customer()
        {
            CustomerPreferences = new HashSet<CustomerPreference>();
            PromoCodes = new HashSet<PromoCode>();
        }

        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50)]
        public string Email { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }
        
        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }

    }
}