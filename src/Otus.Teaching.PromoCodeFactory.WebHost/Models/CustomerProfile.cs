﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            this.CreateMap<Customer, CreateOrEditCustomerRequest>().ReverseMap();
            this.CreateMap<Customer, CustomerShortResponse>().ReverseMap();
            this.CreateMap<Customer, CustomerResponse>()
                .ForMember(c=> c.Preferences, o=> o.MapFrom(m => m.CustomerPreferences.Select(y=>y.Preference).ToList()));
        }
    }
}
