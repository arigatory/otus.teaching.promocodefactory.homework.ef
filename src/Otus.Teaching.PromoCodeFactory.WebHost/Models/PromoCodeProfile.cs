﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeProfile : Profile
    {
        public PromoCodeProfile()
        {
            this.CreateMap<PromoCode, PromoCodeShortResponse>().ReverseMap();
        }
    }
}
